import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import Nav from './Nav';
import HatsList from './HatList';
import HatsForm from './HatForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="/hats" element={<HatsList getHats={props.hats} />} />
          <Route path="/hats/news" element={<HatsForm getHats={props.hats} />} />
          <Route path="shoes">
            <Route index element={<ShoesList />}/>
            <Route path="new" element={<ShoeForm/>}/>
          </Route>
        </Routes>
    </BrowserRouter>
  );
}



export default App;
