# Wardrobify

Team:

* Person 1 - Sherryanne Shen - Hats
* Person 2 - Josh Lao - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.
I have a Shoe model with these components:
    * Manufacturer
    * Name
    * Color
    * Picture Url
    * Bin
I also have a BinVO:
    * Closet Name
    * Import Href
I polled with the poller.py in the shoes directory to get the Bin data over to my shoes.

For the front-end shoes stuff, I added cards for each shoe having every component of the shoe inside along with a Delete button that is fully functional.
On top, there is an Add Shoe button to go to the shoe form, where all of the information can be filled out.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
